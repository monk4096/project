<?php
/**
 * Created by PhpStorm.
 * User: Monk
 * Date: 24.11.2019
 * Time: 19:12
 */

namespace backend\widgets\profile;


use common\models\Profile;
use yii\base\Widget;

class ProfileMini extends Widget
{
    public $model;
    public $directoryAsset;

    public function run()
    {
//        if (is_a($this->model, Profile::class)){
//            return $this->render("profile-mini", ['model'=> $this->model , 'directoryAsset' => $this->directoryAsset,]);
//        }
//        throw new \Exception("ощибка");
        return $this->render("profile-mini", ['model'=> $this->model , 'directoryAsset' => $this->directoryAsset,]);
    }

}