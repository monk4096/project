<?php

?>

<div class="imageMini">
    <img class="img" src="/project2/project/gallery/<?= $model->user?>/mini/<?=$model->filename?>"
         data-full_image_url="/project2/project/gallery/<?= $model->user?>/<?= $model->filename?>" alt="">

    <?php $form = \yii\widgets\ActiveForm::begin([
            'action' => ['images/update/', 'id' => $model->filename],

    ]); ?>

    <?= $form->field($model, 'description')->textInput()->label('Добавьте описание фото.')?>


    <div class="form-group">
        <?= \yii\helpers\Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php \yii\widgets\ActiveForm::end(); ?>

    <p><?=$model->description?></p>
</div>

