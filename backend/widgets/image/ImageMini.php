<?php


namespace backend\widgets\image;


use yii\base\Widget;

class ImageMini extends Widget
{
    public $model;

    public function run()
    {
        if ($this->model->description) {
            return $this->render('image-mini', [
                'model' => $this->model
            ]);
        } else {
            return $this->render('image-update-description', [
                'model' => $this->model
            ]);
        }

    }
}