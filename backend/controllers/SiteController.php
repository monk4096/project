<?php
namespace backend\controllers;

use backend\models\ChatEl;
use backend\models\User;
use common\models\Gallery;
use common\models\Images;
use common\models\Profile;
use common\modules\ws\models\ChatStorage;
use common\modules\ws\models\NewsStorage;
use yii\data\ActiveDataProvider;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\uploadModels\UploadFile;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['chat', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['createprofile', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['profile', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['news', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['gallery', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        \Yii::$app->getView()->params['myAvatar'] = '';
        \Yii::$app->getView()->params['myProfileName'] = '';
        \Yii::$app->getView()->params['avatar'] = '';
        \Yii::$app->getView()->params['profileName'] = '';
        Yii::$app->name = "Project 1.0";
        $model = Profile::findOne(Yii::$app->user->getId());
        if ($model) {
            if (!$model->avatar){
                return $this->redirect('index');
            }
            \Yii::$app->getView()->params['myAvatar'] = $model->avatar;
            \Yii::$app->getView()->params['myProfileName'] = $model->name;
            \Yii::$app->getView()->params['avatar'] = $model->avatar;
            \Yii::$app->getView()->params['profileName'] = $model->name;
        }

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = Profile::findOne(Yii::$app->user->getId());
        if (!isset($model)) {
            return $this->redirect(['createprofile']);
        }

        $chatDataProvider = new ActiveDataProvider([
            'query'  => ChatStorage::getMessage()
        ]);

        $profileList = new ActiveDataProvider([
           'query' => Profile::find()->all(),
        ]);



        \Yii::$app->getView()->params['avatar'] = $model->avatar;
        \Yii::$app->getView()->params['profileName'] = $model->name;

        return $this->render('index',[
            'model' => $model,
            'chatDataProvider' => $chatDataProvider,
            'profileList' => $profileList->query,
            'minImg' => Gallery::getUserCarousel($model->id),
        ]);
    }


    public function actionCreateprofile(){

        $model = new Profile();
        $modelAvatar = new UploadFile();
        if ($model->load(Yii::$app->request->post())) {
            $modelAvatar->uploadedFile = UploadedFile::getInstance($modelAvatar, 'uploadedFile');
            $model->avatar = $modelAvatar->saveAvatar($model->id);
            $model->save();
            return $this->redirect(['index']);
        }
        return $this->render('createProfile', [
           'model' => $model,
            'modelAvatar' => $modelAvatar
        ]);

    }

    public function actionProfile($id) {
        $profile = Profile::findOne($id);

        $chatDataProvider = new ActiveDataProvider([
            'query'  => ChatStorage::getMessage()
        ]);

        $profileList = new ActiveDataProvider([
            'query' => Profile::find()->all(),
        ]);

        

        \Yii::$app->getView()->params['avatar'] = $profile->avatar;
        \Yii::$app->getView()->params['profileName'] = $profile->name;

        return $this->render('profile', [
            'profile' => $profile,
            'profileList' => $profileList->query,
            'chatDataProvider' => $chatDataProvider,
            'minImg' => Gallery::getUserCarousel($id),
            
        ]);
    }


    public function actionNews() {
        $newsDataProvider = NewsStorage::getNews();
        return $this->render('news',[
            'newsDataProvider' => $newsDataProvider,
        ]);
    }

    public function actionGallery(){
        $images = new UploadFile();
        if ($images->load(Yii::$app->request->post())) {
            $images->uploadedFile = UploadedFile::getInstances($images, 'uploadedFile');
            $images->saveImages(Yii::$app->user->getId());
        }

        $dataProvider = new ActiveDataProvider([
           'query' => Images::find()->where(['user' => \Yii::$app->user->getId()])->orderBy('datetime DESC'),
//            'query' => Images::findAll(['user' => \Yii::$app->user->getId()]),
            'pagination' =>['pageSize' => 20]
        ]);

        return $this->render('gallery', [
            'modelImages' => $images,
            'dataProvider' => $dataProvider,
            ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            \yii\console\Application::setInstance(Yii::$app->user);

            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            \yii\console\Application::setInstance(Yii::$app->user);
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionChat() {

        $chatDataProvider = new ActiveDataProvider([
            'query'  => ChatStorage::getMessage()
        ]);

        return $this->render('chat', [
            'chatDataProvider' => $chatDataProvider,
        ]);
    }
}
