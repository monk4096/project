<?php
/**
 * Created by PhpStorm.
 * User: Monk
 * Date: 24.11.2019
 * Time: 18:33
 */
?>

<?php

/* @var $this yii\web\View */

$this->title = 'Профиль';
use yii\bootstrap\Alert;
use yii\bootstrap\Button;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Collapse;
use yii\bootstrap\Carousel;
?>
<div class="site-index">

    <div class="container">
        <div class="row profile-page">
            <div class="col-md-4" style="color: #b7b7b7; min-height: 400px; padding: 10px; border: 1px solid #bdbdbd; box-shadow: 5px 19px 35px;">
                <p><?=$profile->name?></p>
                <img class="img-responsive" src="/project2/project/backend/web/images/avatars/<?=$profile->avatar?>" alt="profile image">
                <?php
                echo Button::widget([
                    'label' => 'Добавить в друзья',
                    'options' => [
                        'class' => 'disabled btn-primary',
                        'style' => 'margin:5px'
                    ]
                ]);

                echo Collapse::widget([
                    'items' => [
                        [
                            'label' => 'Написать сообщение',
                            'content' => 'в разработке',
//                            'content' => '<div class="form">
//                                              <form id="messages" action="" name="messages">
//                                                  <input id="message"  type="text" placeholder="Type message and press enter...">
//                                                  <input type="submit" id="btnSend" class="btn btn-lg btn-success" value="send" >
//                                                  <p style="color: red" id="response"></p>
//                                              </form>
//                                          </div>',
                            'contentOptions' => [],
                            'options' => []
                        ]
                    ]
                ]);
                echo Button::widget([
                    'label' => 'Кнопка для чего нибудь',
                    'options' => [
                        'class' => 'disabled btn-success',
                        'style' => 'margin:5px'
                    ]
                ]);
                ?>
            </div>
            <div class="col-md-2 profile-list" style="color: #9c9ca1; min-height: 500px">
                <h3>Список пользователей:</h3>
                <?php foreach ($profileList as $item) :?>
                    <?php echo \backend\widgets\profile\ProfileMini::widget([
                    'model' => $item
                    ])
                    ?>
                <?php endforeach;?>

            </div>
            <div class="col-md-6" style="color: #9c9ca1; min-height: 500px">
                <h3>Интересы:</h3> <br>
                <p><?=$profile->interests?></p> <hr>
                <h3>О себе:</h3> <br>
                <p><?=$profile->about?></p>
                <hr>
            </div>
            <div class="col-md-12 slider-box">
                <h4>Галерея!</h4>
                <br>
<!--                <p>--><?php //print_r($minImg); ?><!--</p>-->

                <?php echo \yii\bootstrap\Carousel::widget([
                        'items' => $minImg,
                    'controls' => ['&lsaquo;', '&rsaquo;'],
                    'options' => [
                        'style' => 'width:400px;'
                    ]
                ]);
                ?>
                <br>
                <hr>
            </div>
            <div class="col-md-5">
<!--                --><?php
//                echo \common\modules\ws\widgets\chat\Chat::widget([
//                    'model' => $chatDataProvider
//                ]);
//                ?>
            </div>
            <div class="col-md-6">
                <h3>Новости:</h3> <br>
                <p>Пока тут виджеты, друг пригодятся</p>
                <hr>
                <p>Alert</p>
                <?php
                echo Alert::widget([
                    'options' => [
                        'class' => 'alert-info'
                    ],
                    'body' => '<b>Привет</b>, Обратите внимание на эту полезную информацию.'
                ]);
                echo Alert::widget([
                    'options' => [
                        'class' => 'alert-success'
                    ],
                    'body' => '<b>Вы победили!</b> Поздравляем с вашим новым достижением.'
                ]);
                echo Alert::widget([
                    'options' => [
                        'class' => 'alert-warning'
                    ],
                    'body' => '<b>Обратите внимание</b> на эту важную информацию.'
                ]);
                echo Alert::widget([
                    'options' => [
                        'class' => 'alert-danger'
                    ],
                    'body' => '<b>Ошибка!</b> Похоже, что-то сломалось.'
                ]);

                ?>
                <hr>
                <p>Button</p>
                <?php
                echo Button::widget([
                    'label' => 'Default',
                    'options' => [
                        'class' => 'btn-lg btn-default',
                        'style' => 'margin:5px'
                    ], // переопределяем стили bootstrap css своими
                    'tagName' => 'div'
                ]); // по желанию, возможно изменить тег элемента

                echo Button::widget([
                    'label' => 'Primary',
                    'options' => [
                        'class' => 'btn-lg btn-primary',
                        'style' => 'margin:5px'
                    ]
                ]);

                echo Button::widget([
                    'label' => 'Ура!',
                    'options' => [
                        'class' => 'btn-lg btn-success',
                        'style' => 'margin:5px'
                    ]
                ]);

                echo Button::widget([
                    'label' => 'Info',
                    'options' => [
                        'class' => 'btn-lg btn-info',
                        'style' => 'margin:5px'
                    ]
                ]);

                echo Button::widget([
                    'label' => 'Warning',
                    'options' => [
                        'class' => 'btn-lg btn-warning',
                        'style' => 'margin:5px'
                    ]
                ]);

                echo Button::widget([
                    'label' => 'Danger',
                    'options' => [
                        'class' => 'btn-lg btn-danger',
                        'style' => 'margin:5px'
                    ]
                ]);

                echo Button::widget([
                    'label' => 'Link',
                    'options' => [
                        'class' => 'btn-lg btn-link',
                        'style' => 'margin:5px'
                    ]
                ]);

                // disabled
                echo Button::widget([
                    'label' => 'Default',
                    'options' => [
                        'class' => 'disabled btn-default',
                        'style' => 'margin:5px'
                    ], // add a style to overide some default bootstrap css
                    'tagName' => 'div'
                ]); // change the tag used to generate the button if you like

                echo Button::widget([
                    'label' => 'Primary',
                    'options' => [
                        'class' => 'disabled btn-primary',
                        'style' => 'margin:5px'
                    ]
                ]);

                echo Button::widget([
                    'label' => 'Success',
                    'options' => [
                        'class' => 'disabled btn-success',
                        'style' => 'margin:5px'
                    ]
                ]);

                echo Button::widget([
                    'label' => 'Info',
                    'options' => [
                        'class' => 'disabled btn-info',
                        'style' => 'margin:5px'
                    ]
                ]);

                echo Button::widget([
                    'label' => 'Warning',
                    'options' => [
                        'class' => 'disabled btn-warning',
                        'style' => 'margin:5px'
                    ]
                ]);

                echo Button::widget([
                    'label' => 'Danger',
                    'options' => [
                        'class' => 'disabled btn-danger',
                        'style' => 'margin:5px'
                    ]
                ]);

                echo Button::widget([
                    'label' => 'Link',
                    'options' => [
                        'class' => 'disabled btn-link',
                        'style' => 'margin:5px'
                    ]
                ]);

                // mixed size
                echo Button::widget([
                    'label' => 'Large',
                    'options' => [
                        'class' => 'btn-lg btn-default',
                        'style' => 'margin:5px'
                    ], // add a style to overide some default bootstrap css
                    'tagName' => 'div'
                ]); // change the tag used to generate the button if you like

                echo Button::widget([
                    'label' => 'Default',
                    'options' => [
                        'class' => 'btn-primary',
                        'style' => 'margin:5px'
                    ]
                ]);

                echo Button::widget([
                    'label' => 'Small',
                    'options' => [
                        'class' => 'btn-sm btn-success',
                        'style' => 'margin:5px'
                    ]
                ]);

                echo Button::widget([
                    'label' => 'Extra Small',
                    'options' => [
                        'class' => 'btn-xs btn-info',
                        'style' => 'margin:5px'
                    ]
                ]);

                echo Button::widget([
                    'label' => 'Large',
                    'options' => [
                        'class' => 'btn-lg btn-warning',
                        'style' => 'margin:5px'
                    ]
                ]);

                echo Button::widget([
                    'label' => 'Default',
                    'options' => [
                        'class' => 'btn-danger',
                        'style' => 'margin:5px'
                    ]
                ]);
                ?>
                <hr>
                <p>ButtonDropdown</p>
                <?php
                echo ButtonDropdown::widget([
                    'label' => 'Default',
                    'options' => [
                        'class' => 'btn-lg btn-default',
                        'style' => 'margin:5px'
                    ],
                    'dropdown' => [
                        'items' => [
                            [
                                'label' => 'Второе действие',
                                'url' => '#'
                            ],
                            [
                                'label' => 'Третье действие',
                                'url' => '#'
                            ],
                            [
                                'label' => '',
                                'options' => [
                                    'role' => 'presentation',
                                    'class' => 'divider'
                                ]
                            ],
                            [
                                'label' => 'Четвертое действие',
                                'url' => '#'
                            ]
                        ]
                    ]
                ]);

                echo ButtonDropdown::widget([
                    'label' => 'Primary',
                    'options' => [
                        'class' => 'btn-lg btn-primary',
                        'style' => 'margin:5px'
                    ],
                    'dropdown' => [
                        'items' => [
                            [
                                'label' => 'Второе действие',
                                'url' => '#'
                            ],
                            [
                                'label' => 'Третье действие',
                                'url' => '#'
                            ],
                            [
                                'label' => '',
                                'options' => [
                                    'role' => 'presentation',
                                    'class' => 'divider'
                                ]
                            ],
                            [
                                'label' => 'Четвертое действие',
                                'url' => '#'
                            ]
                        ]
                    ]
                ]);

                echo ButtonDropdown::widget([
                    'label' => 'Success',
                    'options' => [
                        'class' => 'btn-lg btn-success',
                        'style' => 'margin:5px'
                    ],
                    'dropdown' => [
                        'items' => [
                            [
                                'label' => 'Второе действие',
                                'url' => '#'
                            ],
                            [
                                'label' => 'Третье действие',
                                'url' => '#'
                            ],
                            [
                                'label' => '',
                                'options' => [
                                    'role' => 'presentation',
                                    'class' => 'divider'
                                ]
                            ],
                            [
                                'label' => 'Четвертое действие',
                                'url' => '#'
                            ]
                        ]
                    ]
                ]);

                echo ButtonDropdown::widget([
                    'label' => 'Info',
                    'options' => [
                        'class' => 'btn-lg btn-info',
                        'style' => 'margin:5px'
                    ],
                    'dropdown' => [
                        'items' => [
                            [
                                'label' => 'Второе действие',
                                'url' => '#'
                            ],
                            [
                                'label' => 'Третье действие',
                                'url' => '#'
                            ],
                            [
                                'label' => '',
                                'options' => [
                                    'role' => 'presentation',
                                    'class' => 'divider'
                                ]
                            ],
                            [
                                'label' => 'Четвертое действие',
                                'url' => '#'
                            ]
                        ]
                    ]
                ]);

                echo ButtonDropdown::widget([
                    'label' => 'Warning',
                    'options' => [
                        'class' => 'btn-lg btn-warning',
                        'style' => 'margin:5px'
                    ],
                    'dropdown' => [
                        'items' => [
                            [
                                'label' => 'Второе действие',
                                'url' => '#'
                            ],
                            [
                                'label' => 'Третье действие',
                                'url' => '#'
                            ],
                            [
                                'label' => '',
                                'options' => [
                                    'role' => 'presentation',
                                    'class' => 'divider'
                                ]
                            ],
                            [
                                'label' => 'Четвертое действие',
                                'url' => '#'
                            ]
                        ]
                    ]
                ]);

                echo ButtonDropdown::widget([
                    'label' => 'Danger',
                    'options' => [
                        'class' => 'btn-lg btn-danger',
                        'style' => 'margin:5px'
                    ],
                    'dropdown' => [
                        'items' => [
                            [
                                'label' => 'Второе действие',
                                'url' => '#'
                            ],
                            [
                                'label' => 'Третье действие',
                                'url' => '#'
                            ],
                            [
                                'label' => '',
                                'options' => [
                                    'role' => 'presentation',
                                    'class' => 'divider'
                                ]
                            ],
                            [
                                'label' => 'Четвертое действие',
                                'url' => '#'
                            ]
                        ]
                    ]
                ]);

                echo ButtonDropdown::widget([
                    'label' => 'Link',
                    'options' => [
                        'class' => 'btn-lg btn-link',
                        'style' => 'margin:5px'
                    ],
                    'dropdown' => [
                        'items' => [
                            [
                                'label' => 'Второе действие',
                                'url' => '#'
                            ],
                            [
                                'label' => 'Третье действие',
                                'url' => '#'
                            ],
                            [
                                'label' => '',
                                'options' => [
                                    'role' => 'presentation',
                                    'class' => 'divider'
                                ]
                            ],
                            [
                                'label' => 'Четвертое действие',
                                'url' => '#'
                            ]
                        ]
                    ]
                ]);

                ?>
                <hr>
                <p>Collapse</p>
                <?php
                echo Collapse::widget([
                    'items' => [
                        [
                            'label' => 'Этот блок открыт, но Вы можете его свернуть.',
                            'content' => 'Содержимое блока, открытого по-умолчанию',
                            // Открыто по-умолчанию
                            'contentOptions' => [
                                'class' => 'in'
                            ]
                        ],
                        [
                            'label' => 'Этот блок свернут, но Вы можете легко открыть его.',
                            'content' => 'Содержимое свернутого блока.',
                            'contentOptions' => [],
                            'options' => []
                        ],
                        [
                            'label' => 'Нажмите здесь',
                            'content' => 'А чего еще Вы ожидали?',
                            'contentOptions' => [],
                            'options' => []
                        ]
                    ]
                ]);


                ?>
            </div>
        </div>
        <div class="row"></div>
    </div>
</div>

