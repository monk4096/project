<?php
/**
 * Created by PhpStorm.
 * User: Monk
 * Date: 05.12.2019
 * Time: 21:13
 */
$this->title = 'Галерея';

?>


<div class="gallery-form">

    <?php $form = \yii\widgets\ActiveForm::begin(); ?>

    <?= $form->field($modelImages, 'uploadedFile[]')->fileInput(['multiple' => true,])?>


    <div class="form-group">
        <?= \yii\helpers\Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php \yii\widgets\ActiveForm::end(); ?>

</div>
<hr>

<div class="gallery galleryPreviewsContainer">
    <h2>Фотографии</h2>
    <?php
    echo \yii\widgets\ListView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['class' => 'gallery-block'],
        'summary' => false,

        'itemView' => function ($model) {
            return \backend\widgets\image\ImageMini::widget(['model' => $model]);
        },
        'viewParams' => [
            'hide' => true
        ],
        'emptyText' => 'Нет фото',
    ])
    ?>
</div>

<script>
    "use strict";

    /**
     * @property {Object} settings Объект с настройками галереи.
     * @property {string} settings.previewSelector Селектор обертки для миниатюр галереи.
     * @property {string} settings.openedImageWrapperClass Класс для обертки открытой картинки.
     * @property {string} settings.openedImageClass Класс открытой картинки.
     * @property {string} settings.openedImageScreenClass Класс для ширмы открытой картинки.
     * @property {string} settings.openedImageCloseBtnClass Класс для картинки кнопки закрыть.
     * @property {string} settings.openedImageCloseBtnSrc Путь до картинки кнопки открыть.
     * 8911-183-93-69
     */
    const gallery = {
        openedImageEl: null,
        arrayImages: null,
        arrayImagesIndex: null,
        settings: {
            previewSelector: '.gallery-block',
            openedImageWrapperClass: 'galleryWrapper',
            openedImageClass: 'galleryWrapper__image',
            openedImageScreenClass: 'galleryWrapper__screen',
            openedImageCloseBtnClass: 'galleryWrapper__close',
            openedImageCloseBtnSrc: 'images/about/gallery/close.png',
            openedImageNextBtnSrc: 'images/about/gallery/right.png',
            openedImageNextBtnClass: 'galleryWrapper__next',
            openedImageBackBtnSrc: 'images/about/gallery/left.png',
            openedImageBackBtnClass: 'galleryWrapper__back',

        },

        /**
         * Инициализирует галерею, ставит обработчик события.
         * @param {Object} userSettings Объект настроек для галереи.
         */
        init(userSettings = {}) {
            // Записываем настройки, которые передал пользователь в наши настройки.
            Object.assign(this.settings, userSettings);

            let cont = document.querySelector(this.settings.previewSelector);
            console.log(cont);

            this.arrayImages = [...cont.getElementsByTagName('img')];


            // Находим элемент, где будут превью картинок и ставим обработчик на этот элемент,
            // при клике на этот элемент вызовем функцию containerClickHandler в нашем объекте
            // gallery и передадим туда событие MouseEvent, которое случилось.
            document
              .querySelector(this.settings.previewSelector)
              .addEventListener('click', event => this.containerClickHandler(event));
        },

        /**
         * Обработчик события клика для открытия картинки.
         * @param {MouseEvent} event Событие клики мышью.
         * @param {HTMLElement} event.target Целевой объект, куда был произведен клик.
         */
        containerClickHandler(event) {
            // Если целевой тег не был картинкой, то ничего не делаем, просто завершаем функцию.
            if (event.target.tagName !== 'IMG') {
                return;
            }

            this.openedImageEl = event.target;

            // Открываем картинку с полученным из целевого тега (data-full_image_url аттрибут).
            this.openImage(event.target.dataset.full_image_url);
        },

        /**
         * Открывает картинку.
         * @param {string} src Ссылка на картинку, которую надо открыть.
         *
         */
        openImage(src) {
            // Получаем контейнер для открытой картинки, в нем находим тег img и ставим ему нужный src.
            let test = this.getScreenContainer().querySelector(`.${this.settings.openedImageClass}`);

            for (let key in this.arrayImages) {
                if (this.arrayImages[key] === this.openedImageEl) {
                    this.arrayImagesIndex = key;
                    this.openedImageEl = this.arrayImages[key];
                    break;
                }
            }
            console.log(test);
            test.src = src;
            console.log(test.src);
            // console.log([...this.arrayImages]);
            // console.log(this.arrayImages.namedItem(this.openedImageEl.name));

            test.onerror = () => test.src = this.settings.canNotOpenedImage;

        },

        /**
         * Возвращает контейнер для открытой картинки, либо создает такой контейнер, если его еще нет.
         * @returns {Element}
         */
        getScreenContainer() {
            // Получаем контейнер для открытой картинки.
            const galleryWrapperElement = document.querySelector(`.${this.settings.openedImageWrapperClass}`);
            // Если контейнер для открытой картинки существует - возвращаем его.
            if (galleryWrapperElement) {
                return galleryWrapperElement;
            }

            // Возвращаем полученный из метода createScreenContainer контейнер.
            return this.createScreenContainer();
        },

        /**
         * Создает контейнер для открытой картинки.
         * @returns {HTMLElement}
         */
        createScreenContainer() {
            // Создаем сам контейнер-обертку и ставим ему класс.
            const galleryWrapperElement = document.createElement('div');
            galleryWrapperElement.classList.add(this.settings.openedImageWrapperClass);

            const backBtn = new Image();
            backBtn.classList.add(this.settings.openedImageBackBtnClass);
            backBtn.src = this.settings.openedImageBackBtnSrc;
            galleryWrapperElement.appendChild(backBtn);

            backBtn.addEventListener('click', () => {
                this.openedImageEl = this.getPrevImage();
                this.openImage(this.openedImageEl.dataset.full_image_url);
            });

            const nextBtn = new Image();
            nextBtn.classList.add(this.settings.openedImageNextBtnClass);
            nextBtn.src = this.settings.openedImageNextBtnSrc;
            galleryWrapperElement.appendChild(nextBtn);

            nextBtn.addEventListener('click', () => {
                this.openedImageEl = this.getNextImage();
                this.openImage(this.openedImageEl.dataset.full_image_url);
            });

            // Создаем контейнер занавеса, ставим ему класс и добавляем в контейнер-обертку.
            const galleryScreenElement = document.createElement('div');
            galleryScreenElement.classList.add(this.settings.openedImageScreenClass);
            galleryWrapperElement.appendChild(galleryScreenElement);

            // Создаем картинку для кнопки закрыть, ставим класс, src и добавляем ее в контейнер-обертку.
            const closeImageElement = new Image();
            closeImageElement.classList.add(this.settings.openedImageCloseBtnClass);
            closeImageElement.src = this.settings.openedImageCloseBtnSrc;
            closeImageElement.addEventListener('click', () => this.close());
            galleryWrapperElement.appendChild(closeImageElement);

            // Создаем картинку, которую хотим открыть, ставим класс и добавляем ее в контейнер-обертку.
            const image = new Image();
            image.classList.add(this.settings.openedImageClass);
            galleryWrapperElement.appendChild(image);
            // image.onerror = () => alert('not ok');

            // Добавляем контейнер-обертку в тег body.

            document.body.appendChild(galleryWrapperElement);


            // Возвращаем добавленный в body элемент, наш контейнер-обертку.
            return galleryWrapperElement;
        },



        getPrevImage() {
            // const nextSibling = this.openedImageEl.previousElementSibling;
            console.log(this.arrayImagesIndex);

            const nextSibling = this.arrayImages[--this.arrayImagesIndex];
            console.log(this.arrayImagesIndex);


            console.log(nextSibling);

            if (this.arrayImagesIndex < 0) {
                this.arrayImagesIndex = parseInt(this.arrayImages.length) - 1;
                return this.arrayImages[this.arrayImagesIndex]
            }
            // return nextSibling ? nextSibling : this.openedImageEl.parentNode.lastElementChild;
            return nextSibling ? nextSibling : this.arrayImages[this.arrayImages.length--];

        },

        getNextImage() {
            // const prevSibling = this.openedImageEl.nextElementSibling;

            const prevSibling = this.arrayImages[++this.arrayImagesIndex];
            this.arrayImagesIndex = ++this.arrayImagesIndex;

            console.log(this.arrayImages);
            console.log(this.arrayImagesIndex);
            console.log(prevSibling);
            // return prevSibling ? prevSibling : this.openedImageEl.parentNode.firstElementChild;

            if (this.arrayImagesIndex > this.arrayImages.length) {
                this.arrayImagesIndex = 0;
                return this.arrayImages[this.arrayImagesIndex]
            } else {
                return prevSibling
            }
            // return prevSibling ? prevSibling : this.arrayImages[0];

        },

        /**
         * Закрывает (удаляет) контейнер для открытой картинки.
         */
        close() {
            document.querySelector(`.${this.settings.openedImageWrapperClass}`).remove();
        }
    };

    // Инициализируем нашу галерею при загрузке страницы.
    window.onload = () => gallery.init({
        canNotOpenedImage: 'images/about/gallery/error.jpg'
    });
</script>
