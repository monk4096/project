<?php
/**
 * Created by PhpStorm.
 * User: Monk
 * Date: 21.11.2019
 * Time: 20:11
 */
?>



<div class="profile-form">

    <?php $form = \yii\widgets\ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->hiddenInput(['value' => Yii::$app->user->getId()])->label(false) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'interests')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'about')->textarea(['maxlength' => true]) ?>

    <?= $form->field($modelAvatar, 'uploadedFile')->fileInput()->label('Загрузить аватурку!')?>


    <div class="form-group">
        <?= \yii\helpers\Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php \yii\widgets\ActiveForm::end(); ?>

</div>

