var conn = new WebSocket('ws://31.134.129.72:9090');

conn.onopen = function(e) {

    // function getCookie(cname) {
    //     var name = cname + "=";
    //     var decodedCookie = decodeURIComponent(document.cookie);
    //     console.log(decodedCookie);
    //     var ca = decodedCookie.split(';');
    //     for(var i = 0; i <ca.length; i++) {
    //         var c = ca[i];
    //         while (c.charAt(0) == ' ') {
    //             c = c.substring(1);
    //         }
    //         if (c.indexOf(name) == 0) {
    //             return c.substring(name.length, c.length);
    //         }
    //     }
    //     return "";
    // }
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        console.log(decodedCookie);
        var ca = decodedCookie.split(';');
        // for(var i = 0; i <ca.length; i++) {
        //     var c = ca[i];
        //     while (c.charAt(0) == ' ') {
        //         c = c.substring(1);
        //     }
        //     if (c.indexOf(name) == 0) {
        //         console.log(c);
        //         return c.substring(name.length, c.length);
        //     }
        // }
        console.log(ca);
        return ca[3];
    }

    console.log("Connection established!");
    var message_send = JSON.stringify({
        'action' : 'register',
        'cookie' : getCookie('_identity-backend')
    });
};

conn.onmessage = function(e) {

    var response = JSON.parse(e.data);
    if (response.type === 'open') {
        console.log('open');
        return;
    }
    var $el = $('.post-item:first').clone();
    $el.find('.post-title').text(response.title);
    $el.find('.post-content').text(response.text);

    $el.find('.author').text('Опубликовал ' + response.from +' дата: '+ response.date);

    $el.prependTo('div.post-wrap');
    // console.log(response.support);
    // console.log(response.date);
    // console.log(e.data);
};

document.forms["add-news"].onsubmit = function() {
    var $text_input = $('#text');
    var $title_input = $('#title');
    if ($text_input.val()) {
        var news_send = JSON.stringify({
            'title': $title_input.val(),
            'text' : $text_input.val(),
            'receiver' : 'in working',
            'action': 'news'
        });
        conn.send(news_send);
        $title_input.val('');
        $text_input.val('');
    } else {
        console.log('пустое сообщение');
    }
    return false;
};