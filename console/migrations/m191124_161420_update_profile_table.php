<?php

use yii\db\Migration;

/**
 * Class m191124_161420_update_profile_table
 */
class m191124_161420_update_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile','name', 'string');
        $this->addColumn('profile','avatar', 'string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191124_161420_update_profile_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191124_161420_update_profile_table cannot be reverted.\n";

        return false;
    }
    */
}
