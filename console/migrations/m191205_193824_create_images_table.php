<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%images}}`.
 */
class m191205_193824_create_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%images}}', [
            'filename' => $this->primaryKey(),
            'description' => $this->string(),
            'user' => $this->integer(),
            'album'=> $this->integer(),
            'datetime' => $this->dateTime()

        ]);
        $this->addForeignKey(
            "fk_images_user", "images", 'user', "user", "id"
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%images}}');
    }
}
