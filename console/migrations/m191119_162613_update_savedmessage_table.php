<?php

use yii\db\Migration;

/**
 * Class m191119_162613_update_savedmessage_table
 */
class m191119_162613_update_savedmessage_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            "fk_savesMessage_sender", "saved_messages", 'sender', "user", "id"
        );
        $this->addForeignKey(
            "fk_savesMessage_receiver", "saved_messages", 'receiver', "user", "id"
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191119_162613_update_savedmessage_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191119_162613_update_savedmessage_table cannot be reverted.\n";

        return false;
    }
    */
}
