<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%saved_messages}}`.
 */
class m191114_203845_create_saved_messages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%saved_messages}}', [
            'id' => $this->primaryKey(),
            'sender' => $this->integer()->notNull(),
            'receiver' => $this->integer(),
            'message' => $this->text(),
            'datetime' => $this->dateTime()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%saved_messages}}');
    }
}
