<?php
/**
 * Created by PhpStorm.
 * User: Monk
 * Date: 26.11.2019
 * Time: 18:25
 */

namespace common\modules\ws\widgets\news;


use yii\base\Widget;

class News extends Widget
{
    public $model;

    public function init()
    {
        parent::init(); // TODO: Change the autogenerated stub
    }

    public function run()
    {
        return $this->render('index', [
            'model' => $this->model,
        ]);
    }

}