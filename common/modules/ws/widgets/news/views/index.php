<?php
/**
 * Created by PhpStorm.
 * User: Monk
 * Date: 26.11.2019
 * Time: 18:22
 */
\common\modules\ws\widgets\news\NewsAsset::register($this);
?>
<div class="add-news_block">
    <form class="add-news" id="add-news" action="#" name="messages">
        <input id="title" style="margin-bottom: 20px" type="text" placeholder="Заголовок новости">
        <input id="text" style="margin-bottom: 20px" type="text" placeholder="Type text and press enter...">
        <input type="submit" id="btnSend" class="btn btn-lg btn-success" value="Опубликовать" >
    </form>
</div>

<div class="post-wrap">

    <hr style="border-bottom: 1px solid #eee">
    <?php foreach ($model as $item): ?>
    <div class="post-item">
        <div class="post-item-wrap">
            <a href="" class="post-link">
<!--                <h4 class="post-title">--><?//= $item['title']?><!--</h4>-->
<!--                <p class="post-content">--><?//=$item['text'] ?><!--</p>-->
<!--                <p class="author">Опубликовал: --><?//= $item['creator']['username'];?><!-- | дата: --><?//= $item['datetime'] ?><!--</p>-->
                <h4 class="post-title"><?= $item->title?></h4>
                <p class="post-content"><?= $item->text; ?></p>
                <p class="author">Опубликовал: <?= $item->creator0->username;?> | дата: <?= $item->datetime ?></p>
            </a>
<!--                                                                                                    int 5 -->
        </div>
    </div>
    <?php endforeach;?>

</div>

