<?php
/**
 * Created by PhpStorm.
 * User: Monk
 * Date: 26.11.2019
 * Time: 18:19
 */

namespace common\modules\ws\widgets\news;


use yii\web\AssetBundle;

class NewsAsset extends AssetBundle
{
    public $sourcePath = '@common/modules/ws/widgets/news/source';
    public $js = [
        'js/news.js'
    ];
    public $css = [
        'css/news.css'
    ];

}