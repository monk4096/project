<?php


namespace common\modules\ws\widgets\chat;



use yii\base\Widget;

class Chat extends Widget
{
    public $model;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('index', [
            'model' => $this->model->query
        ]);
    }

}