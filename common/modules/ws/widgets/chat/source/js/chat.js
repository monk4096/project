"use strict";
var conn = new WebSocket('ws://31.134.129.72:9090');

conn.onopen = function(e) {

    // function getCookie(name) {
    //     var matches = document.cookie.match(new RegExp(
    //       '(?:^|\s)' + name + '=(.*?)(?:;|$)';
    // ));
    //     console.log(matches[1]);
    //     return matches[1];
    // }
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        // for(var i = 0; i <ca.length; i++) {
        //     var c = ca[i];
        //     while (c.charAt(0) == ' ') {
        //         c = c.substring(1);
        //     }
        //     if (c.indexOf(name) == 0) {
        //         console.log(c);
        //         return c.substring(name.length, c.length);
        //     }
        // }
        return ca[3];
    }
    console.log("Connection established!");
    var message_send = JSON.stringify({
        'action' : 'register',
        'cookie' : getCookie('_identity-backend')
    });
    conn.send(message_send);
};

conn.onmessage = function(e) {

    var response = JSON.parse(e.data);
        if (response.type === 'open') {
            console.log('open');
            console.log(response);
            return;
        }
    var $el = $('.clearfix:first').clone();
    $el.find('.message').text(response.message);

    if (response.message_sender === true) {
        $el.prop('class', 'chat_out clearfix');
        $el.find('.author').text('By me  at ' + response.date);
        // $el.addClass('chat_out');
    } else {
        $el.prop('class', 'chat_in clearfix');
        $el.find('.author').text('By '+ response.from + '  at ' + response.date);
    }

    $el.appendTo('div.chat_inner');
    // console.log(response.support);
    // console.log(response.date);
    // console.log(e.data);
};

document.forms["messages"].onsubmit = function() {
    var $message_input = $('#message');
    if ($message_input.val()) {
        $('#response').text('');
        var message_send = JSON.stringify({
            'message' : $message_input.val(),
            'receiver' : 'in working',
            'action': 'support',
            'message_sender' : true
        });
        conn.send(message_send);
        $message_input.val('');
    } else {
        $('#response').text('Введите сообщение');
    }
    return false;
};
