<?php
\common\modules\ws\widgets\chat\ChatAsset::register($this);

?>
<div class="chatbox ">
    <h3>Chat</h3>

    <div class="chat_inner">

        <div class="chat_in clearfix">
            <span class="message"></span>
            <span class="author"></span>
        </div>
        <div class="chat_out clearfix">
            <span class="message"></span>
            <span class="author"></span>
        </div>

        <?php foreach ($model as $msg) :?>
        <div class="chat_in clearfix">
<!--            <span class="message">--><?//=$msg['message']?><!--</span>-->
<!--            <span class="author">By --><?//=$msg['sender'][0]['username']?><!-- at --><?//= $msg['datetime']?><!--</span>-->
            <span class="message"><?=$msg->message?></span>
            <span class="author">By <?= $msg->sender0[0]->username;?> at <?= $msg->datetime?></span>
        </div>
        <?php endforeach; ?>

    </div>
    <div class="form">
        <form id="messages" action="" name="messages">
            <input id="message"  type="text" placeholder="Type message and press enter...">
            <input type="submit" id="btnSend" class="btn btn-lg btn-success" value="send" >
            <p style="color: red" id="response"></p>
        </form>
    </div>
</div>