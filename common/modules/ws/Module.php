<?php

namespace common\modules\ws;

use common\models\User;

/**
 * ws module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'common\modules\ws\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();


        // custom initialization code goes here
    }
}
