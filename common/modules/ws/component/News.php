<?php
/**
 * Created by PhpStorm.
 * User: Monk
 * Date: 26.11.2019
 * Time: 18:56
 */

namespace common\modules\ws\component;


use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use yii\base\Component;

class News extends Component // implements MessageComponentInterface
{
    public function onOpen(ConnectionInterface $conn) {
    }

    public function onMessage(ConnectionInterface $from, $msg) {
    }

    public function onClose(ConnectionInterface $conn) {
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
    }
}