<?php


namespace common\modules\ws\component;

use common\modules\ws\models\ChatClient;
use common\modules\ws\models\ChatStorage;
use backend\models\User;
use common\modules\ws\models\NewsStorage;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use yii\base\Component;

class Chat extends Component implements MessageComponentInterface {
    protected $clients;
    protected $user_id;
    protected $chatRoom;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
        $this->chatRoom = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
//        $this->clients->attach($conn);
        echo "New connection! ({$conn->resourceId})\n";
        $user_info = new ChatClient();
        $user_info->connection = $conn;
        $this->clients->attach($user_info);

    }

    public function onMessage(ConnectionInterface $from, $msg) {

        $numRecv = count($this->clients) - 1;
        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');


        $current_client = $this->findConnection($from);

        $request = json_decode($msg);
        var_dump($request);
        if (isset($request->cookie)) {
            $cookieVal = array();
            preg_match('(\[(\d+))',$request->cookie, $cookieVal);
            $this->user_id =(int)$cookieVal[1] ;
        }


        switch ($request->action) {
            // Здесь можно фильтровать сообщения, для этого в отправке сообщений от пользователя необходимо указывать
            // action и отправлять нужно переводя в строку JSON
            case 'support':
                $saved_message = ChatStorage::saveMessage($this->user_id, null, $request->message);
                $message_model = $saved_message;

                foreach ($this->clients as $client) {
                    if ($from == $client->connection) {
                        $request->message_sender = true;
                    } else {
                        $request->message_sender = false;
                    }
                    $send = json_encode([
                        'from'    => $current_client->name,
                        'date'    => date('H:i'),
                        'message' => $request->message,
                        'message_sender' => $request->message_sender
                    ]);
                    var_dump($send);
                    $client->connection->send($send);
                }
                break;
//                if ( ! empty( $request['message'] ) && $message = trim( $request['message'] ) ) {
//                    $support  = isset( $request['support'] ) ? $request['support'] : false;
//                    $receiver = isset( $request['receiver'] ) ? $request['receiver'] : null;
//
//                    // Здесь можно сохранять сообщения в БД
//                    $saved_message = ChatSupport::saveMessage( $message, $current_client->id, $receiver, $support );
//
//                    $message_model = $saved_message['model'];
//
//                    if ($message_model !== null) {
//
//                        foreach ( $this->clients as $client ) {
//
//                            //Здесь можно делать фильтр, чтобы не отправлять сообщение всем, а конкретным пользователям
//                            //          if ($from!= $client) {
//                            //             $client->send($msg);
//                            //          }
//
//                            $client->connection->send( json_encode( [
//                                'type'    => 'support',
//                                'from'    => $current_client->name,
//                                'date'    => date( 'H:i', $message_model->created_at ), // Стоит обратить внимание, что дата здесь не отформатирована с учетом timezone
//                                'message' => $message,
//                                'photo'   => $current_client->photo,
//                                'support' => $support,
//                            ] ) );
//                        }
//                    } else {
//                        // Выводит сообщение если при сохранении возникли ошибки
//                        $result['message'] = $saved_message['error'];
//                    }
//                } else {
//                    // В случае если сообщение пустое
//                    $result['message'] = 'Enter message';
//                }
//
//                break;
            case 'news':
                $saved_news = NewsStorage::saveNews($this->user_id, $request->title, $request->text);
                foreach ($this->clients as $client) {
                    $send = json_encode([
                        'from'    => $current_client->name,
                        'date'    => date('H:i'),
                        'text' => $request->text,
                        'title' => $request->title
                    ]);
                    var_dump($send);
                    $client->connection->send($send);
                }

                break;
            case 'register':
                // Заполнение данных пользователя при подключении пользователя на основе сессии
                $client          = $this->findConnection( $from );
                $client->id      = $this->user_id;
//                $user = \common\models\User::findIdentityByAccessToken($user_authKey);
            $user=\common\models\User::findOne($this->user_id);
                if ($user !== null) {
                    $client->name = $user->getNameForChat();
                    $client->model = $user;
//                    $client->photo = $user->image;
                }
//                $client->session = $request['session_id'];
                break;
        }

    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $client = $this->findConnection($conn);
        if ($client !== null) {
            $this->clients->detach($client);
        }

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }

    public function findConnection( ConnectionInterface $conn ) {
        // Ищем пользователя, который написал
        foreach ($this->clients as $client) {
            if ($client->connection === $conn) {
                return $client;
            }
        }
        return null;
    }
}