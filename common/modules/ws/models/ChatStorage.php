<?php
/**
 * Created by PhpStorm.
 * User: Monk
 * Date: 16.11.2019
 * Time: 16:53
 */

namespace common\modules\ws\models;


use common\models\SavedMessages;
use yii\base\Model;

class ChatStorage extends Model
{
    public static function  getMessage() {
        return SavedMessages::find()->joinWith('sender0')->all();
    }


    public static function saveMessage ($user_id, $receiver, $message) {
        $model = new SavedMessages();
        $model->sender = $user_id;
        $model->receiver = $receiver;
        $model->message = $message;
        $model->datetime = date("Y-m-d H:i:s");
        $model->save();
        return $model;


    }
}