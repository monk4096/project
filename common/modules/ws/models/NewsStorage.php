<?php
/**
 * Created by PhpStorm.
 * User: Monk
 * Date: 27.11.2019
 * Time: 19:27
 */

namespace common\modules\ws\models;


use common\models\News;
use yii\base\Model;

class NewsStorage extends Model
{
    public static function getNews(){
//        return News::find()->joinWith('creator')->asArray()->all();
        return News::find()->joinWith('creator0', true)->all();

    }

    public static function saveNews($userId, $title, $text){
        $model = new News();
        $model->title = $title;
        $model->text = $text;
        $model->creator = $userId;
        $model->datetime = date("Y-m-d H:i:s");
        $model->save();
        return $model;

    }
}