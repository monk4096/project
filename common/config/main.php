<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-common',
        ],
    ],

    'modules' => [
        'ws' => [
            'class' => 'common\modules\ws\Module',
        ],
    ],
];
