<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "images".
 *
 * @property string $filename
 * @property string|null $description
 * @property int|null $user
 * @property int|null $album
 * @property string|null $datetime
 *
 * @property User $userId
 */
class Images extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['filename'], 'required'], // string 19
            [['user', 'album'], 'integer'],
            [['datetime'], 'safe'],
            [['filename'], 'string', 'max' => 19],
            [['description'], 'string', 'max' => 255],
            [['filename'], 'unique'],
            [['user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'filename' => 'Filename',
            'description' => 'Description',
            'user' => 'User ID',
            'album' => 'Album ID',
            'datetime' => 'Datetime',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserId()
    {
        return $this->hasOne(User::className(), ['id' => 'user']);
    }
}
