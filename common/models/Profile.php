<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property int $id
 * @property int $reputation
 * @property string|null $interests
 * @property string|null $about

 * @property string|null $name
 * @property string|null $avatar
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['avatar'], 'string'],
            [['name'], 'string'],
            [['reputation'], 'safe'],
            [['reputation'], 'integer'],
            [['interests', 'about'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reputation' => 'Reputation',
            'interests' => 'Interests',
            'about' => 'About',
        ];
    }
}
