<?php


namespace common\models;
use yii\base\Model;

class Gallery extends Model
{
    public $images; // []

    public function getMyImagesMini() {
        $model = Images::find()->where(['user' => \Yii::$app->user->getId()])->orderBy('datetime')->all();
        return $model;
    }

    public static function getUserCarousel($id) {

        $images = Images::find()->where(['user' => $id])->all();

        $items = [];
        foreach ($images as $img) {
            $items [] =  [
                'content' => "<img style='width: 400px; max-width: 400px;' src=\"/project2/project/gallery/{$img->user}/$img->filename\" alt=\"\">",
                    'caption' => "<p>{$img->description}</p>",
                    'options' => [],
            ];

        }
//        $items[] = ['content'];
//        foreach ($images as $img) {
//
//            $str="<img style='width: 400px; max-width: 400px;' src=\"/project2/project/gallery/{$img->user}/$img->filename\" alt=\"\">";
//            $items[0]['content'] = str_pad($str, strlen($str));
//        }
        return $items;
    }

}