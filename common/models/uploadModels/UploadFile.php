<?php

namespace common\models\uploadModels;

use common\models\Images;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\imagine\Image;

class UploadFile extends Model
{
    public $uploadedFile;
    private $filename;

    public function rules()
    {
        return [
            [['uploadedFile'], 'required'],
            ['uploadedFile', 'file', 'extensions' => 'png, jpg', 'maxFiles' => 10,]
        ];
    }

    public function saveAvatar($id) {

        $this->filename = "$id.{$this->uploadedFile->getExtension()}";
        $filepath = \Yii::getAlias("@webroot/images/avatars/{$this->filename}");
        $this->uploadedFile->saveAs($filepath);
        Image::thumbnail($filepath, 128, 128)->save(\Yii::getAlias("@webroot/images/avatars/mini/{$this->filename}"));
        return $this->filename;
    }

    public function saveImages($userId){
        $filepath = "../../gallery/{$userId}/mini/";
        if (!is_dir("../../gallery/{$userId}")) {
            mkdir($filepath, 0777, true);
        }
        //        FileHelper::createDirectory("@app/gallery/{$userId}", 0775, true);

        foreach ($this->uploadedFile as $img) {
            $model = new Images();
            $name =\Yii::$app->security->generateRandomString(15);

            $model->filename = "$name.{$img->getExtension()}";

            $img->saveAs("../../gallery/{$userId}/{$model->filename}");
            Image::thumbnail(\Yii::getAlias("@webroot/../../gallery/{$userId}/{$model->filename}"),300,300)
                ->save(\Yii::getAlias("@webroot/../../gallery/{$userId}/mini/{$model->filename}"));
            $model->user = $userId;
            $model->datetime = date("Y-m-d H:i:s");
            $model->save();
        }
    }
}