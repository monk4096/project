<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "saved_messages".
 *
 * @property int $id
 * @property int $sender
 * @property int $receiver
 * @property string $message
 * @property string $datetime
 * @property User $sender0
 * @property User $receiver0
 */
class SavedMessages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'saved_messages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sender', 'datetime'], 'required'],
            [['sender', 'receiver'], 'integer'],
            [['message'], 'string'],
            [['datetime'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sender' => 'Sender',
            'receiver' => 'Receiver',
            'message' => 'Message',
            'datetime' => 'Datetime',
        ];
    }

    public function getSender0() {
        return $this->hasMany(User::class, ['id' => 'sender']);
    }
    public function getReceiver0() {
        return $this->hasOne(User::class, ['id' => 'receiver']);
    }
}
